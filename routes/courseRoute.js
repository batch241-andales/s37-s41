const express = require("express");
const router = express.Router();
const auth = require("../auth");

const courseController = require("../controllers/courseController");

router.post("/", auth.verify, (req,res) => {

	const userData = auth.decode(req.headers.authorization);
	if (userData.isAdmin === true){
		courseController.addCourse(req).then(resultFromController => res.send(resultFromController));}
		else {
			res.send(false)
		}
	})
router.get("/all", (req, res) => {
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController))
})

router.get("/active", (req, res) => {
	courseController.getAllActive().then(resultFromController => res.send(resultFromController))
})

// Specific course
router.get("/:courseId", (req, res) => {
	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController))
})

router.put("/:courseId", auth.verify, (req, res) => {
	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController))
})

router.patch("/:courseId/archive", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	if (userData.isAdmin === true){
		courseController.archiveCourse(req.params, req.body).then(resultFromController => res.send(resultFromController))
	}
	else {
		res.send("Not Authorized")
	}
	
})



module.exports = router;