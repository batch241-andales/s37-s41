const User = require("../models/User");
const Course = require("../models/Course");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// Checking if the email exists in the database
module.exports.checkEmailExists = (reqBody) => {

	return User.find({email:reqBody.email}).then(result => {
		if (result.length > 0){
			return true
		// No duplicate email found / User is not yet registered in database
		} else {
			return false
		}
	})
}

module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		//bcrypt.hashSync(<dataToBeHash>, <saltRound>)
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {
		if(error){
			return false
		} else {
			return user
		}
	})
}

module.exports.loginUser = (reqBody) => {

	// compareSync(dataFromRequestBody, encryptedDataFromDatabase)
	return User.findOne({email: reqBody.email}).then(result => {
		if (result == null){
			return false
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if (isPasswordCorrect){
				return {access: auth.createAccessToken(result)}
			} else {
				return false
			}
		}
	})
}

module.exports.getProfile = (reqBody) => {
	return User.findById(reqBody.id).then(result => {

		let foundUser = {
			_id: result._id,
			firstName: result.firstName,
			lastName: result.lastName,
			email: result.email,
			password: "",
			isAdmin: result.isAdmin,
			mobileNo: result.mobileNo,
			enrollments: result.enrollments,
			__v: result.__v
		}
		return foundUser;

	})
}

// Enroll user to a course
module.exports.enroll = async (data) => {
	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.enrollments.push({courseId: data.courseId});
		return user.save().then((user, error) => {
			if (error){
				return false
			} else {
				return true
			}
		})
	})

	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
		course.enrollees.push({userId: data.userId});
		return course.save().then((course, error) => {
			if (error){
				return false
			} else {
				return true
			}
		})
	})

	if (isUserUpdated && isCourseUpdated){
		return true
	} else {
		return false
	}
}