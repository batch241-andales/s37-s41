const Course = require("../models/Course");
const auth = require("../auth");


module.exports.addCourse = (input) => {
	

	let newCourse = new Course({
		name: input.body.name,
		description: input.body.description,
		price: input.body.price
	})

	return newCourse.save().then((course, error) => {
		if (error){
			return false
		} else {
			return true
		}

	});
} 

// Retrieve all courses
module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	})
}


// Retrieve active

module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then(result => {
		return result;
	})
}

module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result;
	})
}

module.exports.updateCourse = (reqParams, reqBody) => {

	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}
	/*
	Syntax:
	findByIdAndUpdate(docement ID, Updatestobeapplied)
	*/
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {
		if (error){
			return false;
		} else {
			return true;
		}
	})
}


module.exports.archiveCourse = (reqParams, reqBody) => {
	let archiveCourse = {
		isActive: reqBody.isActive
	}

	return Course.findByIdAndUpdate(reqParams.courseId, archiveCourse).then((course, error) => {
		if (error){
			return false
		} else {
			return true
		}
	})
}