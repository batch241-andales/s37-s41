const express = require("express");
const mongoose = require("mongoose")
//Allows our backend application to be available to our frontend application
const cors = require("cors");
const port = 4000;
const userRoute = require("./routes/userRoute");
const courseRoute = require("./routes/courseRoute")
const app = express();


app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());


mongoose.set('strictQuery', true)
mongoose.connect("mongodb+srv://admin123:admin123@cluster0.bfuvdfn.mongodb.net/B241-API?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error"));
db.once("open", () => console.log("We're connected to the Atlas"))

app.use("/users", userRoute);

app.use("/courses", courseRoute);

app.listen(port, () => {
	console.log(`API is now online on port ${port}`)
})